package br.caderneta.vacinacao.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Agente_saude.
 */
@Entity
@Table(name = "agente_saude")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Agente_saude implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Size(max = 11)
    @Column(name = "cpf", length = 11)
    private String cpf;

    @Column(name = "crm")
    private String crm;

    @Column(name = "coren")
    private String coren;

    @Column(name = "inscricao_municipal")
    private String inscricao_municipal;

    @OneToOne
    @JoinColumn(unique = true)
    private User login;

    public Agente_saude(){}

    public Agente_saude(String nome, String cpf, String crm, String coren, String inscricao_municipal) {
        this.nome = nome;
        this.cpf = cpf;
        this.crm = crm;
        this.coren = coren;
        this.inscricao_municipal = inscricao_municipal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Agente_saude nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public Agente_saude cpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCrm() {
        return crm;
    }

    public Agente_saude crm(String crm) {
        this.crm = crm;
        return this;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getCoren() {
        return coren;
    }

    public Agente_saude coren(String coren) {
        this.coren = coren;
        return this;
    }

    public void setCoren(String coren) {
        this.coren = coren;
    }

    public String getInscricao_municipal() {
        return inscricao_municipal;
    }

    public Agente_saude inscricao_municipal(String inscricao_municipal) {
        this.inscricao_municipal = inscricao_municipal;
        return this;
    }

    public void setInscricao_municipal(String inscricao_municipal) {
        this.inscricao_municipal = inscricao_municipal;
    }

    public User getLogin() {
        return login;
    }

    public Agente_saude login(User user) {
        this.login = user;
        return this;
    }

    public void setLogin(User user) {
        this.login = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Agente_saude agente_saude = (Agente_saude) o;
        if (agente_saude.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, agente_saude.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Agente_saude{" +
            "id=" + id +
            ", nome='" + nome + "'" +
            ", cpf='" + cpf + "'" +
            ", crm='" + crm + "'" +
            ", coren='" + coren + "'" +
            ", inscricao_municipal='" + inscricao_municipal + "'" +
            '}';
    }
}
