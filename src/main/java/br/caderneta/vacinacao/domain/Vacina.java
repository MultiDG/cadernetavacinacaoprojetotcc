package br.caderneta.vacinacao.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Vacina.
 */
@Entity
@Table(name = "vacina")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Vacina implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "nome_vacina", nullable = false)
    private String nome_vacina;

    @NotNull
    @Column(name = "data_vacina", nullable = false)
    private LocalDate data_vacina;

    @Column(name = "proxima_vacina")
    private LocalDate proxima_vacina;

    @NotNull
    @Column(name = "lote", nullable = false)
    private String lote;

    @Column(name = "local_vacina")
    private String local_vacina;

    @ManyToOne
    private Paciente vacinado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome_vacina() {
        return nome_vacina;
    }

    public Vacina nome_vacina(String nome_vacina) {
        this.nome_vacina = nome_vacina;
        return this;
    }

    public void setNome_vacina(String nome_vacina) {
        this.nome_vacina = nome_vacina;
    }

    public LocalDate getData_vacina() {
        return data_vacina;
    }

    public Vacina data_vacina(LocalDate data_vacina) {
        this.data_vacina = data_vacina;
        return this;
    }

    public void setData_vacina(LocalDate data_vacina) {
        this.data_vacina = data_vacina;
    }

    public LocalDate getProxima_vacina() {
        return proxima_vacina;
    }

    public Vacina proxima_vacina(LocalDate proxima_vacina) {
        this.proxima_vacina = proxima_vacina;
        return this;
    }

    public void setProxima_vacina(LocalDate proxima_vacina) {
        this.proxima_vacina = proxima_vacina;
    }

    public String getLote() {
        return lote;
    }

    public Vacina lote(String lote) {
        this.lote = lote;
        return this;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getLocal_vacina() {
        return local_vacina;
    }

    public Vacina local_vacina(String local_vacina) {
        this.local_vacina = local_vacina;
        return this;
    }

    public void setLocal_vacina(String local_vacina) {
        this.local_vacina = local_vacina;
    }

    public Paciente getVacinado() {
        return vacinado;
    }

    public Vacina vacinado(Paciente paciente) {
        this.vacinado = paciente;
        return this;
    }

    public void setVacinado(Paciente paciente) {
        this.vacinado = paciente;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vacina vacina = (Vacina) o;
        if (vacina.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, vacina.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Vacina{" +
            "id=" + id +
            ", nome_vacina='" + nome_vacina + "'" +
            ", data_vacina='" + data_vacina + "'" +
            ", proxima_vacina='" + proxima_vacina + "'" +
            ", lote='" + lote + "'" +
            ", local_vacina='" + local_vacina + "'" +
            '}';
    }
}
