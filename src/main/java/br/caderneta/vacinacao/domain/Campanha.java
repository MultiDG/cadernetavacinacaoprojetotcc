package br.caderneta.vacinacao.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Campanha.
 */
@Entity
@Table(name = "campanha")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Campanha implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "idade_minima")
    private int idadeMinima;

    @Column(name = "idade_maxima")
    private int idadeMaxima;

    @Column(name = "data_inicio")
    private LocalDate data_inicio;

    @Column(name = "data_fim")
    private LocalDate data_fim;

    @Column(name = "descricao")
    private String descricao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

       public Campanha nome(String nome) {

        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdadeMinima() {
        return idadeMinima;
    }

    public Campanha idadeMinima(int idadeMinima) {
        return this;
    }

    public void setIdadeMinima(int idadeMinima) {
        this.idadeMinima = idadeMinima;
    }

    public int getIdadeMaxima() {
        return idadeMaxima;
    }

    public Campanha idadeMaxima(int idadeMaxima){
        return this;
    }
    public void setIdadeMaxima(int idadeMaxima) {
        this.idadeMaxima = idadeMaxima;
    }

    public LocalDate getData_inicio() {
        return data_inicio;
    }

    public Campanha data_inicio(LocalDate data_inicio) {
        this.data_inicio = data_inicio;
        return this;
    }

    public void setData_inicio(LocalDate data_inicio) {
        this.data_inicio = data_inicio;
    }

    public LocalDate getData_fim() {
        return data_fim;
    }

    public Campanha data_fim(LocalDate data_fim) {
        this.data_fim = data_fim;
        return this;
    }

    public void setData_fim(LocalDate data_fim) {
        this.data_fim = data_fim;
    }

    public String getDescricao() {
        return descricao;
    }

    public Campanha descricao(String descricao) {
        this.descricao = descricao;
        return this;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Campanha campanha = (Campanha) o;
        if (campanha.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, campanha.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Campanha{" +
            "id=" + id +
            ", nome='" + nome + "'" +
            ", idadeMinima='" + idadeMinima + "'" +
            ", idadeMaxima='" + idadeMaxima + "'" +
            ", data_inicio='" + data_inicio + "'" +
            ", data_fim='" + data_fim + "'" +
            ", descricao='" + descricao + "'" +
            '}';
    }
}
