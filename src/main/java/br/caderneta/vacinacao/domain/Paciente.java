package br.caderneta.vacinacao.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Paciente.
 */
@Entity
@Table(name = "paciente")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Paciente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Size(max = 11)
    @Column(name = "cpf", length = 11)
    private String cpf;


    @Column(name = "nome_pai")
    private String nome_pai;

    @Column(name = "nome_mae")
    private String nome_mae;

    @Column(name = "data_nascimento")
    private LocalDate data_nascimento;

    @Column(name = "endereco")
    private String endereco;

    @Column(name = "telefone")
    private String telefone;

    @Column(name = "sexo")
    private String sexo;

    @OneToOne
    @JoinColumn(unique = true)
    private User login;

    public Paciente() {

    }

    public Paciente(String nome, String cpf, String nome_pai, String nome_mae, LocalDate data_nascimento, String endereco, String telefone, String sexo) {
        this.nome = nome;
        this.cpf = cpf;
        this.nome_pai = nome_pai;
        this.nome_mae = nome_mae;
        this.data_nascimento = data_nascimento;
        this.endereco = endereco;
        this.telefone = telefone;
        this.sexo = sexo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Paciente nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public Paciente cpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome_pai() {
        return nome_pai;
    }

    public Paciente nome_pai(String nome_pai) {
        this.nome_pai = nome_pai;
        return this;
    }

    public void setNome_pai(String nome_pai) {
        this.nome_pai = nome_pai;
    }

    public String getNome_mae() {
        return nome_mae;
    }

    public Paciente nome_mae(String nome_mae) {
        this.nome_mae = nome_mae;
        return this;
    }

    public void setNome_mae(String nome_mae) {
        this.nome_mae = nome_mae;
    }

    public LocalDate getData_nascimento() {
        return data_nascimento;
    }

    public Paciente data_nascimento(LocalDate data_nascimento) {
        this.data_nascimento = data_nascimento;
        return this;
    }

    public void setData_nascimento(LocalDate data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public String getEndereco() {
        return endereco;
    }

    public Paciente endereco(String endereco) {
        this.endereco = endereco;
        return this;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getTelefone() {
        return telefone;
    }

    public Paciente telefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getSexo() {
        return sexo;
    }

    public Paciente sexo(String sexo) {
        this.sexo = sexo;
        return this;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public User getLogin() {
        return login;
    }

    public Paciente login(User user) {
        this.login = user;
        return this;
    }

    public void setLogin(User user) {
        this.login = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Paciente paciente = (Paciente) o;
        if (paciente.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, paciente.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Paciente{" +
            "id=" + id +
            ", nome='" + nome + "'" +
            ", cpf='" + cpf + "'" +
            ", nome_pai='" + nome_pai + "'" +
            ", nome_mae='" + nome_mae + "'" +
            ", data_nascimento='" + data_nascimento + "'" +
            ", endereco='" + endereco + "'" +
            ", telefone='" + telefone + "'" +
            ", sexo='" + sexo + "'" +
            '}';
    }
}
