package br.caderneta.vacinacao.repository;

import br.caderneta.vacinacao.domain.Paciente;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Paciente entity.
 */
@SuppressWarnings("unused")
public interface PacienteRepository extends JpaRepository<Paciente,Long> {

    Paciente findByLoginLogin(String login);

    @Query("select paciente from Paciente paciente where paciente.cpf = :cpf")
    Optional<Paciente> findOneByCpf(@Param(value = "cpf") String cpf);

}
