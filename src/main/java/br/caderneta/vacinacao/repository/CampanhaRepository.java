package br.caderneta.vacinacao.repository;

import br.caderneta.vacinacao.domain.Campanha;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Campanha entity.
 */
@SuppressWarnings("unused")
public interface CampanhaRepository extends JpaRepository<Campanha,Long> {

}
