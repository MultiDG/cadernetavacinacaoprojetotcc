package br.caderneta.vacinacao.repository;

import br.caderneta.vacinacao.domain.Agente_saude;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Agente_saude entity.
 */
@SuppressWarnings("unused")
public interface Agente_saudeRepository extends JpaRepository<Agente_saude,Long> {

    Agente_saude findByLoginLogin(String login);
}
