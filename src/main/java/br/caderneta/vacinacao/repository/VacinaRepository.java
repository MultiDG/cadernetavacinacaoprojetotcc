package br.caderneta.vacinacao.repository;

import br.caderneta.vacinacao.domain.User;
import br.caderneta.vacinacao.domain.Vacina;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the Vacina entity.
 */
@SuppressWarnings("unused")
public interface VacinaRepository extends JpaRepository<Vacina,Long> {

    @Query("select vacina from Vacina vacina where vacina.vacinado.login.login = ?#{principal.username}")
    List<Vacina> findByVacinadoIsCurrentUser();

    @Query("select vacina from Vacina vacina where vacina.vacinado.id = :id")
    List<Vacina> findByPorPaciente(@Param("id") Long id);

}
