package br.caderneta.vacinacao.service;

import br.caderneta.vacinacao.domain.Campanha;
import br.caderneta.vacinacao.domain.Paciente;
import br.caderneta.vacinacao.domain.User;
import br.caderneta.vacinacao.repository.CampanhaRepository;
import br.caderneta.vacinacao.repository.PacienteRepository;
import br.caderneta.vacinacao.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Campanha.
 */
@Service
@Transactional
public class CampanhaService {

    private final Logger log = LoggerFactory.getLogger(CampanhaService.class);

    private final CampanhaRepository campanhaRepository;

    private final UserRepository userRepository;

    private final PacienteRepository pacienteRepository;

    public CampanhaService(CampanhaRepository campanhaRepository, UserRepository userRepository, PacienteRepository pacienteRepository) {
        this.campanhaRepository = campanhaRepository;
        this.userRepository = userRepository;
        this.pacienteRepository = pacienteRepository;
    }

    /**
     * Save a campanha.
     *
     * @param campanha the entity to save
     * @return the persisted entity
     */
    public Campanha save(Campanha campanha) {
        log.debug("Request to save Campanha : {}", campanha);
        Campanha result = campanhaRepository.save(campanha);
        return result;
    }

    /**
     *  Get all the campanhas.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Campanha> findAll() {
        log.debug("Request to get all Campanhas");
        List<Campanha> result = campanhaRepository.findAll();

        return result;
    }

    @Transactional(readOnly = true)
    public List<Campanha> findCampanhasPorUsuario(String loginUsuario) {
        log.debug("Request to get all Campanhas");
        Paciente paciente = pacienteRepository.findByLoginLogin(loginUsuario);
        Period period = Period.between(paciente.getData_nascimento(), LocalDate.now());
        int idadePaciente = period.getYears();

        List<Campanha> result = campanhaRepository.findAll();


        return result.stream()
            .filter((campanha) -> idadePaciente >= campanha.getIdadeMinima() && idadePaciente <= campanha.getIdadeMaxima() && campanha.getData_fim().isAfter(LocalDate.now()))//int
            .collect(Collectors.toList());
    }

    /**
     *  Get one campanha by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Campanha findOne(Long id) {
        log.debug("Request to get Campanha : {}", id);
        Campanha campanha = campanhaRepository.findOne(id);
        return campanha;
    }

    /**
     *  Delete the  campanha by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Campanha : {}", id);
        campanhaRepository.delete(id);
    }
    public void deleteForDate(Campanha campanha){
         campanha = campanhaRepository.findOne(campanha.getId());
        if(campanha.getData_fim().atTime(23,59).isBefore(LocalDate.now().atStartOfDay())){
            campanhaRepository.delete(campanha);
        }
    }
}
