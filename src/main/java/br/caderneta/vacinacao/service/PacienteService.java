package br.caderneta.vacinacao.service;

import br.caderneta.vacinacao.domain.Paciente;
import br.caderneta.vacinacao.repository.PacienteRepository;
import br.caderneta.vacinacao.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Paciente.
 */
@Service
@Transactional
public class PacienteService {

    private final Logger log = LoggerFactory.getLogger(PacienteService.class);

    private final PacienteRepository pacienteRepository;

    private final UserRepository userRepository;

    public PacienteService(PacienteRepository pacienteRepository, UserRepository userRepository) {
        this.pacienteRepository = pacienteRepository;
        this.userRepository = userRepository;
    }
    /**
     * Save a paciente.
     *
     * @param paciente the entity to save
     * @return the persisted entity
     */
    public Paciente save(Paciente paciente) {
        log.debug("Request to save Paciente : {}", paciente);
        Paciente result = pacienteRepository.save(paciente);
        return result;
    }

    /**
     *  Get all the pacientes.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Paciente> findAll() {
        log.debug("Request to get all Pacientes");
        List<Paciente> result = pacienteRepository.findAll();

        return result;
    }

    /**
     *  Get one paciente by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Paciente findOne(Long id) {
        log.debug("Request to get Paciente : {}", id);
        Paciente paciente = pacienteRepository.findOne(id);
        return paciente;
    }


    /**
     *  Get one paciente by cpf.
     *
     *  @param cpf the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Paciente> findOneByCpf(String cpf) {
        log.debug("Request to get Paciente : {}", cpf);
        Optional<Paciente> paciente = pacienteRepository.findOneByCpf(cpf);
        return paciente;
    }


    /**
     *  Delete the  paciente by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Paciente : {}", id);
        Paciente paciente = pacienteRepository.findOne(id);
        pacienteRepository.delete(paciente.getId());
        userRepository.findOneByLogin(paciente.getLogin().getLogin()).ifPresent(user -> {
            userRepository.delete(user);
            log.debug("Deleted User: {}", user);
        });
    }
}
