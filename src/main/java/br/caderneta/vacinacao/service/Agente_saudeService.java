package br.caderneta.vacinacao.service;

import br.caderneta.vacinacao.domain.Agente_saude;
import br.caderneta.vacinacao.repository.Agente_saudeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service Implementation for managing Agente_saude.
 */
@Service
@Transactional
public class Agente_saudeService {

    private final Logger log = LoggerFactory.getLogger(Agente_saudeService.class);

    private final Agente_saudeRepository agente_saudeRepository;

    public Agente_saudeService(Agente_saudeRepository agente_saudeRepository) {
        this.agente_saudeRepository = agente_saudeRepository;
    }

    /**
     * Save a agente_saude.
     *
     * @param agente_saude the entity to save
     * @return the persisted entity
     */
    public Agente_saude save(Agente_saude agente_saude) {
        log.debug("Request to save Agente_saude : {}", agente_saude);
        Agente_saude result = agente_saudeRepository.save(agente_saude);
        return result;
    }

    /**
     *  Get all the agente_saudes.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Agente_saude> findAll() {
        log.debug("Request to get all Agente_saudes");
        List<Agente_saude> result = agente_saudeRepository.findAll();

        return result;
    }

    /**
     *  Get one agente_saude by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Agente_saude findOne(Long id) {
        log.debug("Request to get Agente_saude : {}", id);
        Agente_saude agente_saude = agente_saudeRepository.findOne(id);
        return agente_saude;
    }

    /**
     *  Delete the  agente_saude by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Agente_saude : {}", id);
        agente_saudeRepository.delete(id);
    }

    public Agente_saude findByLogin(String login) {
        return agente_saudeRepository.findByLoginLogin(login);
    }
}
