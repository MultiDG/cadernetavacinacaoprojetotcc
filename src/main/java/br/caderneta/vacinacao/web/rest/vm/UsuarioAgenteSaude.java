package br.caderneta.vacinacao.web.rest.vm;

import br.caderneta.vacinacao.domain.Agente_saude;
import br.caderneta.vacinacao.domain.Authority;
import br.caderneta.vacinacao.domain.User;
import br.caderneta.vacinacao.repository.AuthorityRepository;
import br.caderneta.vacinacao.repository.UserRepository;
import br.caderneta.vacinacao.security.AuthoritiesConstants;
import br.caderneta.vacinacao.service.UserService;
import br.caderneta.vacinacao.service.util.RandomUtil;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by MariDg on 18/06/2017.
 */
public class UsuarioAgenteSaude {

    private Long idAgente;
    private Long idUsuario;
    private String nome;
    private String login;
    private String password;
    @Email
    private String email;
    @Size(max = 11)
    private String cpf;

    private String crm;

    private String coren;

    private AuthorityRepository authorityRepository;

    private UserRepository userRepository;

    private String inscricao_municipal;
    @Size(min = 2, max = 5)
    private String langKey;

    public UsuarioAgenteSaude(AuthorityRepository authorityRepository) {
        this.authorityRepository = authorityRepository;
    }
    public  UsuarioAgenteSaude(){};

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getCoren() {
        return coren;
    }

    public void setCoren(String coren) {
        this.coren = coren;
    }

    public String getInscricao_municipal() {
        return inscricao_municipal;
    }

    public void setInscricao_municipal(String inscricao_municipal) {
        this.inscricao_municipal = inscricao_municipal;
    }

    public String getLangKey() {
        return langKey;
    }

    public Long getIdAgente() {
        return idAgente;
    }

    public void setIdAgente(Long idAgente) {
        this.idAgente = idAgente;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }
    //Esse eh o momento que o usuario eh criado, passando os dados que vieram da tela (login, password, etc) pra dentro dele
    //que sera usado pra criacao do usuario no banco
    public ManagedUserVM montarUsuario() {
        return new ManagedUserVM(login, password, email, langKey);
    }

    public Agente_saude montarAgente() {
        return new Agente_saude(nome, cpf, coren, crm, inscricao_municipal);
    }

    //Com paciente eh a mesma coisa. Crio ele aqui com os dados da tela, que sera usado pra criacao do paciente no banco
    public Agente_saude montarAgenteSaude() {

        User user = new User();
        user.setId(idUsuario);
//        user.setEmail(email);
//        user.setLogin(login);
//        user.setActivated(true);
//        user.setActivationKey(RandomUtil.generateActivationKey());
//
//            Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
//            Set<Authority> authorities = new HashSet<>();
//            authorities.add(authority);
//        user.setAuthorities(authorities);


        Agente_saude agente = new Agente_saude();
        agente.setLogin(user);
        agente.setId(idAgente);
        agente.setCrm(crm);
        agente.setCpf(cpf);
        agente.setCoren(coren);
        agente.setInscricao_municipal(inscricao_municipal);
        agente.setNome(nome);
        return agente;
    }

    public User salvarUsuario(String email, String login){

        User user = new User();
            user.setEmail(email);
            user.setLogin(login);
            user.setActivated(true);
            user.setPassword("12345");
            user.setActivationKey(RandomUtil.generateActivationKey());
            Authority authority = authorityRepository.findOne(AuthoritiesConstants.ADMIN);
            Set<Authority> authorities = new HashSet<>();
            authorities.add(authority);
            user.setAuthorities(authorities);

            userRepository.save(user);

            return user;
    }


}
