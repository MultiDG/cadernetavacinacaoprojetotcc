package br.caderneta.vacinacao.web.rest.vm;

import br.caderneta.vacinacao.config.Constants;
import br.caderneta.vacinacao.domain.Paciente;
import org.hibernate.validator.constraints.Email;

import javax.persistence.Column;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * Created by DG on 16/06/2017.
 */
public class UsuarioPaciente {


    Long id;

    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    private String login;

    @Size(max = 50)
    private String nome;

    @Email
    @Size(min = 5, max = 100)
    private String email;

    @Size(min = 4, max = 100)
    private String password;

    @Size(max = 11)
    private String cpf;

    private String nome_pai;

    private String nome_mae;

    private LocalDate data_nascimento;

    private String endereco;

    private String telefone;

    private String sexo;

    @Size(min = 2, max = 5)
    private String langKey;

    //Esse eh o momento que o usuario eh criado, passando os dados que vieram da tela (login, password, etc) pra dentro dele
    //que sera usado pra criacao do usuario no banco
    public ManagedUserVM montarUsuario() {
        return new ManagedUserVM(login, password, email, langKey);
    }

    //Com paciente eh a mesma coisa. Crio ele aqui com os dados da tela, que sera usado pra criacao do paciente no banco
    public Paciente montarPaciente() {
        return new Paciente(nome, cpf, nome_pai, nome_mae, data_nascimento, endereco, telefone, sexo);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setNome_pai(String nome_pai) {
        this.nome_pai = nome_pai;
    }

    public void setNome_mae(String nome_mae) {
        this.nome_mae = nome_mae;
    }

    public void setData_nascimento(LocalDate data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void setLangKey(String langKey) {
        this.langKey = langKey;
    }


    public String getCpf() {
        return cpf;
    }
}


