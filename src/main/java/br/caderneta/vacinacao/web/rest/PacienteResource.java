package br.caderneta.vacinacao.web.rest;

import br.caderneta.vacinacao.repository.PacienteRepository;
import br.caderneta.vacinacao.security.SecurityUtils;
import br.caderneta.vacinacao.service.UserService;
import br.caderneta.vacinacao.web.rest.vm.UsuarioPaciente;
import com.codahale.metrics.annotation.Timed;
import br.caderneta.vacinacao.domain.Paciente;
import br.caderneta.vacinacao.service.PacienteService;
import br.caderneta.vacinacao.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Paciente.
 */
@RestController
@RequestMapping("/api")
public class PacienteResource {

    private final Logger log = LoggerFactory.getLogger(PacienteResource.class);

    private static final String ENTITY_NAME = "paciente";

    private final PacienteService pacienteService;

    private final PacienteRepository pacienteRepository;

    private final UserService userService;

    public PacienteResource(PacienteService pacienteService, PacienteRepository pacienteRepository, UserService userService) {
        this.pacienteService = pacienteService;
        this.pacienteRepository = pacienteRepository;
        this.userService = userService;
    }

    /**
     * POST  /pacientes : Create a new paciente.
     *
     * @param paciente the paciente to create
     * @return the ResponseEntity with status 201 (Created) and with body the new paciente, or with status 400 (Bad Request) if the paciente has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pacientes")
    @Timed
    public ResponseEntity<Paciente> createPaciente(@Valid @RequestBody Paciente paciente) throws URISyntaxException {
        log.debug("REST request to save Paciente : {}", paciente);
        if (paciente.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new paciente cannot already have an ID")).body(null);
        }
        Paciente result = pacienteService.save(paciente);
        return ResponseEntity.created(new URI("/api/pacientes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pacientes : Updates an existing paciente.
     *
     * @param paciente the paciente to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated paciente,
     * or with status 400 (Bad Request) if the paciente is not valid,
     * or with status 500 (Internal Server Error) if the paciente couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pacientes")
    @Timed
    public ResponseEntity<Paciente> updatePaciente(@Valid @RequestBody Paciente paciente) throws URISyntaxException {
        log.debug("REST request to update Paciente : {}", paciente);
        if (paciente.getId() == null) {
            return createPaciente(paciente);
        }
        paciente.getLogin().setId(paciente.getLogin().getId());
        Paciente result = pacienteService.save(paciente);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paciente.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pacientes : get all the pacientes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pacientes in body
     */
    @GetMapping("/pacientes")
    @Timed
    public List<Paciente> getAllPacientes() {
        log.debug("REST request to get all Pacientes");
        return pacienteService.findAll();
    }


    @GetMapping("/paciente/logado")
    @Timed
    public ResponseEntity<UsuarioPaciente> getPacienteLogado() {
        Paciente paciente = pacienteRepository.findByLoginLogin(SecurityUtils.getCurrentUserLogin());
        if(paciente == null) {
            paciente = new Paciente();
            paciente.setLogin(userService.getUserWithAuthorities());
        }
        UsuarioPaciente usuarioPaciente = new UsuarioPaciente();

        usuarioPaciente.setId(paciente.getId());
        usuarioPaciente.setLogin(paciente.getLogin().getLogin());
        usuarioPaciente.setEmail(paciente.getLogin().getEmail());
        usuarioPaciente.setNome(paciente.getNome());
        usuarioPaciente.setCpf(paciente.getCpf());
        usuarioPaciente.setData_nascimento(paciente.getData_nascimento());
        usuarioPaciente.setEndereco(paciente.getEndereco());
        usuarioPaciente.setTelefone(paciente.getTelefone());

        usuarioPaciente.setSexo(paciente.getSexo());

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(usuarioPaciente));
    }


    /**
     * GET  /pacientes/:id : get the "id" paciente.
     *
     * @param id the id of the paciente to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the paciente, or with status 404 (Not Found)
     */
    @GetMapping("/pacientes/{id}")
    @Timed
    public ResponseEntity<Paciente> getPaciente(@PathVariable Long id) {
        log.debug("REST request to get Paciente : {}", id);
        Paciente paciente = pacienteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paciente));
    }

    /**
     * DELETE  /pacientes/:id : delete the "id" paciente.
     *
     * @param id the id of the paciente to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pacientes/{id}")
    @Timed
    public ResponseEntity<Void> deletePaciente(@PathVariable Long id) {
        log.debug("REST request to delete Paciente : {}", id);
        pacienteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }


}
