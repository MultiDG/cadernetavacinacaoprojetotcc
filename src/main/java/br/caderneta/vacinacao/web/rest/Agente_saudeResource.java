package br.caderneta.vacinacao.web.rest;

import br.caderneta.vacinacao.domain.User;
import br.caderneta.vacinacao.repository.UserRepository;
import br.caderneta.vacinacao.security.SecurityUtils;
import br.caderneta.vacinacao.service.UserService;
import br.caderneta.vacinacao.web.rest.vm.ManagedUserVM;
import br.caderneta.vacinacao.web.rest.vm.UsuarioAgenteSaude;
import com.codahale.metrics.annotation.Timed;
import br.caderneta.vacinacao.domain.Agente_saude;
import br.caderneta.vacinacao.service.Agente_saudeService;
import br.caderneta.vacinacao.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Agente_saude.
 */
@RestController
@RequestMapping("/api")
public class Agente_saudeResource {

    private final Logger log = LoggerFactory.getLogger(Agente_saudeResource.class);

    private static final String ENTITY_NAME = "agente_saude";

    private final Agente_saudeService agente_saudeService;

    private final UserRepository userRepository;

    private final UserService userService;

    public Agente_saudeResource(Agente_saudeService agente_saudeService, UserService userService, UserRepository userRepository) {
        this.agente_saudeService = agente_saudeService;
        this.userService = userService;
        this.userRepository = userRepository;
    }

    /**
     * POST  /agente-saudes : Create a new agente_saude.
     *
     * @param agente_saude the agente_saude to create
     * @return the ResponseEntity with status 201 (Created) and with body the new agente_saude, or with status 400 (Bad Request) if the agente_saude has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/agente-saudes")
    @Timed
    public ResponseEntity<Agente_saude> saveAgente_saude(@Valid @RequestBody UsuarioAgenteSaude agente_saude) throws URISyntaxException {
        log.debug("REST request to save Agente_saude : {}", agente_saude);

        Agente_saude agenteSaude = agente_saude.montarAgenteSaude();
        Agente_saude result = agente_saudeService.save(agenteSaude);
        return ResponseEntity.created(new URI("/api/agente-saudes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    /**
     * GET  /agente-saudes : get all the agente_saudes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of agente_saudes in body
     */
    @GetMapping("/agente-saudes")
    @Timed
    public List<Agente_saude> getAllAgente_saudes() {
        log.debug("REST request to get all Agente_saudes");
        return agente_saudeService.findAll();
    }

    /**
     * GET  /agente-saudes/:id : get the "id" agente_saude.
     *
     * @param id the id of the agente_saude to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the agente_saude, or with status 404 (Not Found)
     */
    @GetMapping("/agente-saudes/{id}")
    @Timed
    public ResponseEntity<Agente_saude> getAgente_saude(@PathVariable Long id) {
        log.debug("REST request to get Agente_saude : {}", id);
        Agente_saude agente_saude = agente_saudeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(agente_saude));
    }

    @GetMapping("/agente-saudes/logado")
    @Timed
    public ResponseEntity<UsuarioAgenteSaude> getAgenteLogado() {
        Agente_saude agente_saude = agente_saudeService.findByLogin(SecurityUtils.getCurrentUserLogin());
        if(agente_saude == null) {
            agente_saude = new Agente_saude();
            agente_saude.setLogin(userService.getUserWithAuthorities());
        }
        UsuarioAgenteSaude usuarioAgenteSaude = new UsuarioAgenteSaude();
        usuarioAgenteSaude.setIdAgente(agente_saude.getId());
        usuarioAgenteSaude.setIdUsuario(agente_saude.getLogin().getId());
        usuarioAgenteSaude.setEmail(agente_saude.getLogin().getEmail());
        usuarioAgenteSaude.setLogin(agente_saude.getLogin().getLogin());

        usuarioAgenteSaude.setNome(agente_saude.getNome());
        usuarioAgenteSaude.setCpf(agente_saude.getCpf());
        usuarioAgenteSaude.setCoren(agente_saude.getCoren());
        usuarioAgenteSaude.setCrm(agente_saude.getCrm());
        usuarioAgenteSaude.setInscricao_municipal(agente_saude.getInscricao_municipal());

        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(usuarioAgenteSaude));
    }


    /**
     * DELETE  /agente-saudes/:id : delete the "id" agente_saude.
     *
     * @param id the id of the agente_saude to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/agente-saudes/{id}")
    @Timed
    public ResponseEntity<Void> deleteAgente_saude(@PathVariable Long id) {
        log.debug("REST request to delete Agente_saude : {}", id);
        agente_saudeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
