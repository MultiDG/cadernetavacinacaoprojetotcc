/**
 * View Models used by Spring MVC REST controllers.
 */
package br.caderneta.vacinacao.web.rest.vm;
