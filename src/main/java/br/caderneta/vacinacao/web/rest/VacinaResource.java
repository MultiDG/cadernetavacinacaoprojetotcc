package br.caderneta.vacinacao.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.caderneta.vacinacao.domain.Vacina;

import br.caderneta.vacinacao.repository.VacinaRepository;
import br.caderneta.vacinacao.web.rest.util.HeaderUtil;
import br.caderneta.vacinacao.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * REST controller for managing Vacina.
 */
@RestController
@RequestMapping("/api")
public class VacinaResource {

    private final Logger log = LoggerFactory.getLogger(VacinaResource.class);

    private static final String ENTITY_NAME = "vacina";

    private final VacinaRepository vacinaRepository;


    public VacinaResource(VacinaRepository vacinaRepository) {
        this.vacinaRepository = vacinaRepository;
    }

    /**
     * POST  /vacinas : Create a new vacina.
     *
     * @param vacina the vacina to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vacina, or with status 400 (Bad Request) if the vacina has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vacinas")
    @Timed
    public ResponseEntity<Vacina> createVacina(@Valid @RequestBody Vacina vacina) throws URISyntaxException {
        log.debug("REST request to save Vacina : {}", vacina);
        if (vacina.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new vacina cannot already have an ID")).body(null);
        }
        Vacina result = vacinaRepository.save(vacina);
        return ResponseEntity.created(new URI("/api/vacinas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /vacinas : Updates an existing vacina.
     *
     * @param vacina the vacina to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vacina,
     * or with status 400 (Bad Request) if the vacina is not valid,
     * or with status 500 (Internal Server Error) if the vacina couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vacinas")
    @Timed
    public ResponseEntity<Vacina> updateVacina(@Valid @RequestBody Vacina vacina) throws URISyntaxException {
        log.debug("REST request to update Vacina : {}", vacina);
        if (vacina.getId() == null) {
            return createVacina(vacina);
        }
        Vacina result = vacinaRepository.save(vacina);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vacina.getId().toString()))
            .body(result);
    }

    /**
     * GET  /vacinas : get all the vacinas.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vacinas in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @GetMapping("/vacinas")
    @Timed
    public ResponseEntity<List<Vacina>> getAllVacinas(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of Vacinas");
        Page<Vacina> page = vacinaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vacinas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vacinas/:id : get the "id" vacina.
     *
     * @param id the id of the vacina to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vacina, or with status 404 (Not Found)
     */
    @GetMapping("/vacinas/{id}")
    @Timed
    public ResponseEntity<Vacina> getVacina(@PathVariable Long id) {
        log.debug("REST request to get Vacina : {}", id);
        Vacina vacina = vacinaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vacina));
    }

    /**
     * DELETE  /vacinas/:id : delete the "id" vacina.
     *
     * @param id the id of the vacina to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vacinas/{id}")
    @Timed
    public ResponseEntity<Void> deleteVacina(@PathVariable Long id) {
        log.debug("REST request to delete Vacina : {}", id);
        vacinaRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/vacinas/minhas-vacinas")
    @Timed
    public ResponseEntity<List<Vacina>> getAllVacinasPorUsuarioLogado() throws URISyntaxException {
        log.debug("REST request to get a page of Vacinas");

        List<Vacina> vacinas = vacinaRepository.findByVacinadoIsCurrentUser();
        return new ResponseEntity<>(vacinas, HttpStatus.OK);
    }

    @RequestMapping(value = "vacinas/vacinas-por-periodo", method = RequestMethod.GET)
    public ResponseEntity<List<Vacina>> vacinasPorPerido(@RequestParam(value = "dataInicio") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dataInicio,
                                                         @RequestParam(value="dataFim")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dataFim) {

        List<Vacina> result = vacinaRepository.findAll();

        List<Vacina>  vacinas =  result.stream()
            .filter((vacina) ->
                vacina.getData_vacina().isEqual(dataInicio) || vacina.getData_vacina().isAfter(dataInicio)
                    && vacina.getData_vacina().isBefore(dataFim) || vacina.getData_vacina().isEqual(dataFim))

            .collect(Collectors.toList());
        return new ResponseEntity<>(vacinas, HttpStatus.OK);
    }

}
