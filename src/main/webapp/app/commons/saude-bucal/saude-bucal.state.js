(function () {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('saude-bucal', {
                parent: 'entity',
                url: '/saude-bucal',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'app/commons/saude-bucal/saude-bucal.html',
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('saude-bucal');
                        return $translate.refresh();
                    }]
                }
            })
    }
})();
