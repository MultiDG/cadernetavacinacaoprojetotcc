(function () {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('evite-acidentes', {
                parent: 'entity',
                url: '/evite-acidentes',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'app/commons/evite-acidentes/evite-acidentes.html',
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('evite-acidentes');
                        return $translate.refresh();
                    }]
                }
            })
    }
})();
