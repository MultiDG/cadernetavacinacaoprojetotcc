(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('dicas-saude', {
                parent: 'app',
                url: '/dicas-saude',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'app/commons/dicas-saude/dicas-saude.html',
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('dicas-saude');
                        return $translate.refresh();
                    }]
                }

            })
    }
})();
