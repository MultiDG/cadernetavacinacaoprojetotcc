(function () {
    'use strict';
    angular
        .module('cadernetavacinacaoApp')
        .factory('Report', Report);

    Report.$inject = ['$resource'];

    function Report($resource) {

        return $resource('api/vacinas/vacinas-por-periodo', {}, {
            'get': { method: 'GET', isArray: true },
            'vacinasporperiodo': {
                method: 'GET',
                transformResponse: function (data) {
                    return data;
                }
            },

        });
    }
})();
