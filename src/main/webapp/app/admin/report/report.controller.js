(function(){
    'use strict'
    angular
        .module('cadernetavacinacaoApp')
        .controller('reportController', reportController);

        reportController.$inject = ['$scope', 'Report'];

        function reportController($scope, Report) {
            var vm = this;
            vm.datePickerOpenStatus = {};
            vm.openCalendar = openCalendar;
            vm.datePickerOpenStatus.dataInicio = false;
            vm.datePickerOpenStatus.dataFim = false;
            vm.vacinasPorPeriodo = vacinasPorPeriodo;

            $scope.$watch('vm.dataFim', function() {
            if(vm.dataFim < vm.dataInicio){
                  alert('Data Inicio não pode ser maior do que a data Fim');
                  
            }
        });


        function vacinasPorPeriodo(){

          vm.vacinas = Report.get({
              dataInicio: new Date(vm.dataInicio).toISOString().substring(0,10),
              dataFim: new Date(vm.dataFim).toISOString().substring(0,10)});
    }

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
            }
        }
})();
