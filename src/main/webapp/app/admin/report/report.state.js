(function () {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
            .state('report', {
                parent: 'app',
                url: '/report',
                data: {
                    authorities: []
                },
                views: {
                    'content@': {
                        templateUrl: 'app/admin/report/report.html',
                        controller: 'reportController',
                        controllerAs: 'vm'
                    }
                },
                
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('saude-bucal');
                        return $translate.refresh();
                    }]
                }
            })
    }
})();
