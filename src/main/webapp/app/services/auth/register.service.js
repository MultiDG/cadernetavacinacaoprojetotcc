(function () {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .factory('Register', Register);

    Register.$inject = ['$resource'];

    function Register ($resource) {
        return $resource('api/register', {}, {});
    }
})();
