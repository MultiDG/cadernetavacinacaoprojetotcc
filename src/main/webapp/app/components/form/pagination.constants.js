(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
