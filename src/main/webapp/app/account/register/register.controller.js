(function () {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('RegisterController', RegisterController);


    RegisterController.$inject = ['$translate', '$timeout', 'Auth', 'LoginService', 'uibDateParser'];

    function RegisterController($translate, $timeout, Auth, LoginService, uibDateParser) {
        var vm = this;

        vm.doNotMatch = null;
        vm.error = null;
        vm.errorUserExists = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.registerAccount = {};
        vm.success = null;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.datePickerOpenStatus.data_nascimento = false;
        vm.dateFormat = "dd/MM/yyyy";
        
        function openCalendar(date) {
            vm.datePickerOpenStatus[date] = true;
        }

        $timeout(function () { angular.element('#login').focus(); });

        function register() {
            if (vm.registerAccount.password !== vm.confirmPassword) {
                vm.doNotMatch = 'ERROR';
            } else {
                vm.registerAccount.langKey = $translate.use();
                vm.doNotMatch = null;
                vm.error = null;
                vm.errorUserExists = null;
                vm.errorEmailExists = null;
                vm.errorCpfEmUso = null;

                Auth.createAccount(vm.registerAccount).then(function () {
                    vm.success = 'OK';
                }).catch(function (response) {
                    vm.success = null;
                    if (response.status === 400 && response.data === 'login already in use') {
                        vm.errorUserExists = 'ERROR';
                    } else if (response.status === 400 && response.data === 'e-mail address already in use') {
                        vm.errorEmailExists = 'ERROR';
                    }
                    else if (response.status === 400 && response.data === 'O cpf ja está em uso') {
                        vm.errorCpfEmUso = 'ERROR';
                    }
                    else {
                        vm.error = 'ERROR';
                    }
                });
            }
        }
    }
})();
