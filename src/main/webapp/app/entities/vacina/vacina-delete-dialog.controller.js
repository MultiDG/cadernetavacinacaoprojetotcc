(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('VacinaDeleteController',VacinaDeleteController);

    VacinaDeleteController.$inject = ['$uibModalInstance', 'entity', 'Vacina'];

    function VacinaDeleteController($uibModalInstance, entity, Vacina) {
        var vm = this;

        vm.vacina = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Vacina.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
