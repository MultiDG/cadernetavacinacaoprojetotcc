(function () {
    'use strict';
    angular
        .module('cadernetavacinacaoApp')
        .factory('Vacina', Vacina);

    Vacina.$inject = ['$resource', 'DateUtils'];

    function Vacina($resource, DateUtils) {
        var resourceUrl = 'api/vacinas/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.data_vacina = DateUtils.convertLocalDateFromServer(data.data_vacina);
                        data.proxima_vacina = DateUtils.convertLocalDateFromServer(data.proxima_vacina);
                    }
                    return data;
                }
            },

            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.data_vacina = DateUtils.convertLocalDateToServer(copy.data_vacina);
                    copy.proxima_vacina = DateUtils.convertLocalDateToServer(copy.proxima_vacina);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.data_vacina = DateUtils.convertLocalDateToServer(copy.data_vacina);
                    copy.proxima_vacina = DateUtils.convertLocalDateToServer(copy.proxima_vacina);
                    return angular.toJson(copy);
                }
            },
            'minhas': {
                method: 'GET',
                isArray: true,
                url: 'api/vacinas/minhas-vacinas',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.data_vacina = DateUtils.convertLocalDateFromServer(data.data_vacina);
                        data.proxima_vacina = DateUtils.convertLocalDateFromServer(data.proxima_vacina);
                    }
                    return data;
                }
            },
        });
    }
})();
