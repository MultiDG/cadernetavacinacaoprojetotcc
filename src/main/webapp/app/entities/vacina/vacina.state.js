(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('vacina', {
            parent: 'entity',
            url: '/vacina?page&sort&search',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'cadernetavacinacaoApp.vacina.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/vacina/vacinas.html',
                    controller: 'VacinaController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('vacina');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('vacina-detail', {
            parent: 'vacina',
            url: '/vacina/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'cadernetavacinacaoApp.vacina.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/vacina/vacina-detail.html',
                    controller: 'VacinaDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('vacina');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Vacina', function($stateParams, Vacina) {
                    return Vacina.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'vacina',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('vacina-detail.edit', {
            parent: 'vacina-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vacina/vacina-dialog.html',
                    controller: 'VacinaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Vacina', function(Vacina) {
                            return Vacina.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('vacina.new', {
            parent: 'vacina',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vacina/vacina-dialog.html',
                    controller: 'VacinaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nome_vacina: null,
                                data_vacina: null,
                                proxima_vacina: null,
                                lote: null,
                                local_vacina: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('vacina', null, { reload: 'vacina' });
                }, function() {
                    $state.go('vacina');
                });
            }]
        })
        .state('vacina.edit', {
            parent: 'vacina',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vacina/vacina-dialog.html',
                    controller: 'VacinaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Vacina', function(Vacina) {
                            return Vacina.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('vacina', null, { reload: 'vacina' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('vacina.delete', {
            parent: 'vacina',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/vacina/vacina-delete-dialog.html',
                    controller: 'VacinaDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Vacina', function(Vacina) {
                            return Vacina.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('vacina', null, { reload: 'vacina' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
