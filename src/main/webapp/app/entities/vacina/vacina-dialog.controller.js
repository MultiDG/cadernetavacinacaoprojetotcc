(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('VacinaDialogController', VacinaDialogController);

    VacinaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Vacina', 'Paciente'];

    function VacinaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Vacina, Paciente) {
        var vm = this;
        vm.vacinas =  [
            "Antitetanica", 
            "BCG",
            "Hepatite B",
            "Antipolio",
            "Tetravalente DTP + Hib",
            "Febre Amarela",
            "Tríplice Viral",
            "Outras"
            ];

        vm.tipovacina = ['Nova', 'Antiga'];
        vm.vacina = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.paciente = Paciente.query();
        vm.dateFormat="dd/MM/yyyy";
        vm.vacina.data_vacina = moment().toDate('DD/MM/YYYY');
       

        vm.setProximaVacina = function(vacina){
            var proximaVacina = moment();
            switch(vacina !== null){
                case vacina == "Antitetanica" : 
                        proximaVacina = moment(vm.vacina.data_vacina).add(10, 'years');
                        vm.vacina.proxima_vacina = proximaVacina.toDate('DD/MM/YYYY');
                        break;
                 case vacina == "BCG" : 
                        proximaVacina = moment(vm.vacina.data_vacina).add(7, 'years');
                        vm.vacina.proxima_vacina = proximaVacina.toDate('DD/MM/YYYY');
                        break;
                 case vacina == "Febre Amarela" : 
                        proximaVacina = moment(vm.vacina.data_vacina).add(3, 'years');
                        vm.vacina.proxima_vacina = proximaVacina.toDate('DD/MM/YYYY');
                        break;
                 case vacina == "Hepatite B" : 
                        proximaVacina = moment(vm.vacina.data_vacina).add(8, 'years');
                        vm.vacina.proxima_vacina = proximaVacina.toDate('DD/MM/YYYY');
                        break;
                 case vacina == "Antipolio" : 
                        proximaVacina = moment(vm.vacina.data_vacina).add(5, 'years');
                        vm.vacina.proxima_vacina = proximaVacina.toDate('DD/MM/YYYY');   
                        break;                     
                 case vacina == "Tríplice viral" : 
                        proximaVacina = moment(vm.vacina.data_vacina).add(2, 'years');
                        vm.vacina.proxima_vacina = proximaVacina.toDate('DD/MM/YYYY');  
                        break;
                 case vacina == "Tetravalente DTP + Hib" : 
                        proximaVacina = moment(vm.vacina.data_vacina).add(6, 'years');
                        vm.vacina.proxima_vacina = proximaVacina.toDate('DD/MM/YYYY');  
                        break;       
                case vacina == "Outras" : 
                        vm.vacina.proxima_vacina = null;
                        break;         
                              
            }
            }
        

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.vacina.id !== null) {
                Vacina.update(vm.vacina, onSaveSuccess, onSaveError);
            } else {
                Vacina.save(vm.vacina, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cadernetavacinacaoApp:vacinaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.data_vacina = false;
        vm.datePickerOpenStatus.proxima_vacina = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
        vm.postos = [ "CMS Ana Gonzaga", "CMS Carlos Alberto Nascimento", "CMS Mario Vitor de Assis Pacheco", "Praça Major Vieira de Melo, sem número"];
    }
})();
