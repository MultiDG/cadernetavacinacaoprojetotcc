(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('VacinaDetailController', VacinaDetailController);

    VacinaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Vacina', 'Paciente'];

    function VacinaDetailController($scope, $rootScope, $stateParams, previousState, entity, Vacina, Paciente) {
        var vm = this;

        vm.vacina = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('cadernetavacinacaoApp:vacinaUpdate', function(event, result) {
            vm.vacina = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
