(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('VacinaController', VacinaController);

    VacinaController.$inject = ['$state', 'Vacina', 'ParseLinks', 'AlertService', 'paginationConstants', 'pagingParams'];

    function VacinaController($state, Vacina, ParseLinks, AlertService, paginationConstants, pagingParams) {

        var vm = this;
        vm.loadPage = loadPage;
        vm.predicate = pagingParams.predicate;
        vm.reverse = pagingParams.ascending;
        vm.transition = transition;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.ordernarPor = function(campo){

    vm.criterioOrdenacao = campo;

    //quando for false, vira true e quando é true vira false
    vm.direcaoOrdenacao = !vm.direcaoOrdenacao;

}

        loadAll();

        function loadAll () {
            Vacina.query({
                page: pagingParams.page - 1,
                size: vm.itemsPerPage,
                sort: sort()
            }, onSuccess, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'id') {
                    result.push('id');
                }
                return result;
            }
            function onSuccess(data, headers) {
                vm.links = ParseLinks.parse(headers('link'));
                vm.totalItems = headers('X-Total-Count');
                vm.queryCount = vm.totalItems;
                vm.vacinas = data;
                
                vm.page = pagingParams.page;
            }
            function onError(error) {
                AlertService.error(error.data.message);
            }
        }

        function loadPage(page) {
            vm.page = page;
            vm.transition();
        }

        function transition() {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                search: vm.currentSearch
            });
        }
    }
})();
