(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('Agente_saudeDeleteController',Agente_saudeDeleteController);

    Agente_saudeDeleteController.$inject = ['$uibModalInstance', 'entity', 'Agente_saude'];

    function Agente_saudeDeleteController($uibModalInstance, entity, Agente_saude) {
        var vm = this;

        vm.agente_saude = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Agente_saude.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
