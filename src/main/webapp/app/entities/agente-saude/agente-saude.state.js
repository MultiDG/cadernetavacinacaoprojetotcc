(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('agente-saude', {
            parent: 'entity',
            url: '/agente-saude',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'cadernetavacinacaoApp.agente_saude.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/agente-saude/agente-saudes.html',
                    controller: 'Agente_saudeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('agente_saude');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('agente-saude-detail', {
            parent: 'agente-saude',
            url: '/agente-saude/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'cadernetavacinacaoApp.agente_saude.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/agente-saude/agente-saude-detail.html',
                    controller: 'Agente_saudeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('agente_saude');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Agente_saude', function($stateParams, Agente_saude) {
                    return Agente_saude.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'agente-saude',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('agente-saude-detail.edit', {
            parent: 'agente-saude-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/agente-saude/agente-saude-dialog.html',
                    controller: 'Agente_saudeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Agente_saude', function(Agente_saude) {
                            return Agente_saude.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('agente-saude.new', {
            parent: 'agente-saude',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/agente-saude/agente-saude-dialog.html',
                    controller: 'Agente_saudeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nome: null,
                                cpf: null,
                                crm: null,
                                coren: null,
                                inscricao_municipal: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('agente-saude', null, { reload: 'agente-saude' });
                }, function() {
                    $state.go('agente-saude');
                });
            }]
        })
        .state('agente-saude.edit', {
            parent: 'agente-saude',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/agente-saude/agente-saude-dialog.html',
                    controller: 'Agente_saudeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Agente_saude', function(Agente_saude) {
                            return Agente_saude.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('agente-saude', null, { reload: 'agente-saude' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('agente-saude.delete', {
            parent: 'agente-saude',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/agente-saude/agente-saude-delete-dialog.html',
                    controller: 'Agente_saudeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Agente_saude', function(Agente_saude) {
                            return Agente_saude.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('agente-saude', null, { reload: 'agente-saude' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
