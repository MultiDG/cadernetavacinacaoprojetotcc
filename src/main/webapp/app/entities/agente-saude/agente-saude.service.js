(function() {
    'use strict';
    angular
        .module('cadernetavacinacaoApp')
        .factory('Agente_saude', Agente_saude);

    Agente_saude.$inject = ['$resource'];

    function Agente_saude ($resource) {
        var resourceUrl =  'api/agente-saudes/:id';

        return $resource(resourceUrl, {}, {
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'logado': { method: 'GET', isArray: false, url: 'api/agente-saudes/logado' },
           'saveAgente': {
                method: 'POST',
                url: 'api/agente-saudes/save'
            },
             'saveUsuarioAgente': {
                method: 'POST',
                url: 'api/register/agente'
            }
        });
    }
})();
