(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('Agente_saudeDialogController', Agente_saudeDialogController);

    Agente_saudeDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Agente_saude', 'User', 'Auth'];

    function Agente_saudeDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Agente_saude, User, Auth) {
        var vm = this;

        vm.agente_saude = entity;
        vm.clear = clear;
        vm.save = save;
        vm.users = User.query();
        vm.exibe = exibe;

        function exibe(){
            console.log(entity);
        }
     $timeout(function () { angular.element('#login').focus(); });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            //  Auth.createAccount(vm.agente_saude).then(function (data) {
            //     vm.error = null;
            //     vm.success = 'OK';
                
            // }).catch(function () {
            //     vm.success = null;
            //     vm.error = 'ERROR';
            // });
            Agente_saude.saveUsuarioAgente(vm.agente_saude, onSaveSuccess, onSaveError);
        }

        function onSaveSuccess (result) {
            $scope.$emit('cadernetavacinacaoApp:agente_saudeUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
