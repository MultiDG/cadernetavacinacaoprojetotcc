(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('Agente_saudeDetailController', Agente_saudeDetailController);

    Agente_saudeDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Agente_saude', 'User'];

    function Agente_saudeDetailController($scope, $rootScope, $stateParams, previousState, entity, Agente_saude, User) {
        var vm = this;

        vm.agente_saude = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('cadernetavacinacaoApp:agente_saudeUpdate', function(event, result) {
            vm.agente_saude = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
