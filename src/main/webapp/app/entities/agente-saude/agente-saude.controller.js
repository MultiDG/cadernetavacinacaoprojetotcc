(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('Agente_saudeController', Agente_saudeController);

    Agente_saudeController.$inject = ['Agente_saude'];

    function Agente_saudeController(Agente_saude) {

        var vm = this;

        vm.agente_saudes = [];
                         vm.ordernarPor = function(campo){

    vm.criterioOrdenacao = campo;

    //quando for false, vira true e quando é true vira false
    vm.direcaoOrdenacao = !vm.direcaoOrdenacao;

}

        loadAll();

        function loadAll() {
            Agente_saude.query(function(result) {
                vm.agente_saudes = result;
                vm.searchQuery = null;
            });
        }
    }
})();
