(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('PacienteController', PacienteController);

    PacienteController.$inject = ['Paciente'];

    function PacienteController(Paciente) {

        var vm = this;

        vm.pacientes = [];
                vm.ordernarPor = function(campo){

    vm.criterioOrdenacao = campo;

    //quando for false, vira true e quando é true vira false
    vm.direcaoOrdenacao = !vm.direcaoOrdenacao;

}

        loadAll();

        function loadAll() {
            Paciente.query(function(result) {
                vm.pacientes = result;
                vm.searchQuery = null;
            });
        }
    }
})();
