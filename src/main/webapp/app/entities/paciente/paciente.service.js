(function() {
    'use strict';
    angular
        .module('cadernetavacinacaoApp')
        .factory('Paciente', Paciente);

    Paciente.$inject = ['$resource', 'DateUtils'];

    function Paciente ($resource, DateUtils) {
        var resourceUrl =  'api/pacientes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.data_nascimento = DateUtils.convertLocalDateFromServer(data.data_nascimento);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.data_nascimento = DateUtils.convertLocalDateToServer(copy.data_nascimento);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.data_nascimento = DateUtils.convertLocalDateToServer(copy.data_nascimento);
                    return angular.toJson(copy);
                }
            },
            'logado': { method: 'GET', isArray: false, url: 'api/paciente/logado' },
        });
    }
})();
