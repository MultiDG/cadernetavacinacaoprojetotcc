(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('CampanhaController', CampanhaController);

    CampanhaController.$inject = ['Campanha'];

    function CampanhaController(Campanha) {

        var vm = this;

        vm.campanhas = [];
         vm.ordernarPor = function(campo){

                 vm.criterioOrdenacao = campo;

    //quando for false, vira true e quando é true vira false
    vm.direcaoOrdenacao = !vm.direcaoOrdenacao;

}

        loadAll();

        function loadAll() {
            Campanha.query(function(result) {
                vm.campanhas = result;
                vm.searchQuery = null;
            });
        }
    }
})();
