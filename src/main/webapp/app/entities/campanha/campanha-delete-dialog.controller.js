(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('CampanhaDeleteController',CampanhaDeleteController);

    CampanhaDeleteController.$inject = ['$uibModalInstance', 'entity', 'Campanha'];

    function CampanhaDeleteController($uibModalInstance, entity, Campanha) {
        var vm = this;

        vm.campanha = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Campanha.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
