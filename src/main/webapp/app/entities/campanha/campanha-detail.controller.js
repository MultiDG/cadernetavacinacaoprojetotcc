(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('CampanhaDetailController', CampanhaDetailController);

    CampanhaDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Campanha'];

    function CampanhaDetailController($scope, $rootScope, $stateParams, previousState, entity, Campanha) {
        var vm = this;

        vm.campanha = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('cadernetavacinacaoApp:campanhaUpdate', function(event, result) {
            vm.campanha = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
