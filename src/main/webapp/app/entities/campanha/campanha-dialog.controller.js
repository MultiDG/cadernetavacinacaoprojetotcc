(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('CampanhaDialogController', CampanhaDialogController);

    CampanhaDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Campanha'];

    function CampanhaDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Campanha) {
        var vm = this;

        vm.campanha = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.dateFormat = "dd/MM/yyyy";


        $scope.$watch('vm.campanha.data_fim', function() {
            if(vm.campanha.data_fim < vm.campanha.data_inicio){
                  alert('Data Inicio não pode ser maior do que a data Fim');
                  
            }
        });

        $scope.$watch('vm.campanha.idadeMaxima', function() {
            if(vm.campanha.idadeMaxima < vm.campanha.idadeMinima){
                  alert('Idade minima não pode ser maior do que a idade maxima');
            }
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.campanha.id !== null) {
                Campanha.update(vm.campanha, onSaveSuccess, onSaveError);
            } else {
                Campanha.save(vm.campanha, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('cadernetavacinacaoApp:campanhaUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.data_inicio = false;
        vm.datePickerOpenStatus.data_fim = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
