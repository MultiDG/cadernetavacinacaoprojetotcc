(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('campanha', {
            parent: 'entity',
            url: '/campanha',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'cadernetavacinacaoApp.campanha.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/campanha/campanhas.html',
                    controller: 'CampanhaController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('campanha');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('campanha-detail', {
            parent: 'campanha',
            url: '/campanha/{id}',
            data: {
                authorities: ['ROLE_ADMIN'],
                pageTitle: 'cadernetavacinacaoApp.campanha.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/campanha/campanha-detail.html',
                    controller: 'CampanhaDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('campanha');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Campanha', function($stateParams, Campanha) {
                    return Campanha.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'campanha',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('campanha-detail.edit', {
            parent: 'campanha-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/campanha/campanha-dialog.html',
                    controller: 'CampanhaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Campanha', function(Campanha) {
                            return Campanha.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('campanha.new', {
            parent: 'campanha',
            url: '/new',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/campanha/campanha-dialog.html',
                    controller: 'CampanhaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                nome: null,
                                idade_publico: null,
                                data_inicio: null,
                                data_fim: null,
                                descricao: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('campanha', null, { reload: 'campanha' });
                }, function() {
                    $state.go('campanha');
                });
            }]
        })
        .state('campanha.edit', {
            parent: 'campanha',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/campanha/campanha-dialog.html',
                    controller: 'CampanhaDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Campanha', function(Campanha) {
                            return Campanha.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('campanha', null, { reload: 'campanha' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('campanha.delete', {
            parent: 'campanha',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_ADMIN']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/campanha/campanha-delete-dialog.html',
                    controller: 'CampanhaDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Campanha', function(Campanha) {
                            return Campanha.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('campanha', null, { reload: 'campanha' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
