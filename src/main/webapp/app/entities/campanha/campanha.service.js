(function () {
    'use strict';
    angular
        .module('cadernetavacinacaoApp')
        .factory('Campanha', Campanha);

    Campanha.$inject = ['$resource', 'DateUtils'];

    function Campanha($resource, DateUtils) {
        var resourceUrl = 'api/campanhas/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true },
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.data_inicio = DateUtils.convertLocalDateFromServer(data.data_inicio);
                        data.data_fim = DateUtils.convertLocalDateFromServer(data.data_fim);
                    }
                    return data;
                }
            },
            'minhas': {
                method: 'GET',
                isArray: true,
                url: 'api/campanhas/minhas-campanhas',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.data_inicio = DateUtils.convertLocalDateFromServer(data.data_inicio);
                        data.data_fim = DateUtils.convertLocalDateFromServer(data.data_fim);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.data_inicio = DateUtils.convertLocalDateToServer(copy.data_inicio);
                    copy.data_fim = DateUtils.convertLocalDateToServer(copy.data_fim);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.data_inicio = DateUtils.convertLocalDateToServer(copy.data_inicio);
                    copy.data_fim = DateUtils.convertLocalDateToServer(copy.data_fim);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
