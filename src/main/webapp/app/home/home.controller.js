(function () {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', 'Principal', 'LoginService', 'Vacina','Campanha', '$state'];

    function HomeController($scope, Principal, LoginService, Vacina, Campanha, $state) {
        var vm = this;
        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.minhasCampanhas = null;
        vm.minhasVacinas = null;
        vm.dataAtual = new Date();
        $scope.$on('authenticationSuccess', function () {
            getAccount();
        });

        getAccount();
       
           function getAccount() {
            Principal.identity().then(function (account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
                vm.minhasVacinas = Vacina.minhas() ? Vacina.minhas() : null;
                vm.campanhas = Campanha.query();

                vm.campanhas.$promise.then(function(data)
                {
                    console.log(data.id)
                });
                if(vm.account.authorities[0] == "ROLE_USER"){
                    $state.go('home.campanhas');
                    vm.minhasCampanhas = Campanha.minhas();
                }  
                else {
                    $state.go("home");
                }        
            });
        }
        function register() {
            $state.go('register');
        }
    }
})();
