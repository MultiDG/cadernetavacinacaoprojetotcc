(function() {
    'use strict';

    angular
        .module('cadernetavacinacaoApp')
        .controller('HomeCampanhaDialog', HomeCampanhaDialog);

    HomeCampanhaDialog.$inject = ['$scope', '$stateParams', '$state', '$uibModalInstance', 'Campanha'];

    function HomeCampanhaDialog($scope, $stateParams, $state, $uibModalInstance, Campanha) {
        var vm = this;
      
        vm.clear = clear;
        vm.minhasCampanhas = Campanha.minhas();

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
