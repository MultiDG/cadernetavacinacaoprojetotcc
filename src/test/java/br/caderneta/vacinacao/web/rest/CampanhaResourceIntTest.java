package br.caderneta.vacinacao.web.rest;

import br.caderneta.vacinacao.CadernetavacinacaoApp;

import br.caderneta.vacinacao.domain.Campanha;
import br.caderneta.vacinacao.repository.CampanhaRepository;
import br.caderneta.vacinacao.service.CampanhaService;
import br.caderneta.vacinacao.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CampanhaResource REST controller.
 *
 * @see CampanhaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CadernetavacinacaoApp.class)
public class CampanhaResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final int DEFAULT_IDADEMINIMA = 5;
    private static final int UPDATED_IDADEMINIMA = 10;

    private static final int DEFAULT_IDADEMAXIMA = 12;
    private static final int UPDATED_IDADEMAXIMA = 18;

    private static final LocalDate DEFAULT_DATA_INICIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_INICIO = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATA_FIM = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_FIM = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    @Autowired
    private CampanhaRepository campanhaRepository;

    @Autowired
    private CampanhaService campanhaService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCampanhaMockMvc;

    private Campanha campanha;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CampanhaResource campanhaResource = new CampanhaResource(campanhaService);
        this.restCampanhaMockMvc = MockMvcBuilders.standaloneSetup(campanhaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Campanha createEntity(EntityManager em) {
        Campanha campanha = new Campanha()
              .nome(DEFAULT_NOME)
            .data_inicio(DEFAULT_DATA_INICIO)
            .data_fim(DEFAULT_DATA_FIM)
            .idadeMinima(DEFAULT_IDADEMINIMA)
            .idadeMaxima(DEFAULT_IDADEMAXIMA)
            .descricao(DEFAULT_DESCRICAO);

        return campanha;
    }

    @Before
    public void initTest() {
        campanha = createEntity(em);
    }

    @Test
    @Transactional
    public void createCampanha() throws Exception {
        int databaseSizeBeforeCreate = campanhaRepository.findAll().size();

        // Create the Campanha

        restCampanhaMockMvc.perform(post("/api/campanhas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campanha)))
            .andExpect(status().isCreated());

        // Validate the Campanha in the database
        List<Campanha> campanhaList = campanhaRepository.findAll();
        assertThat(campanhaList).hasSize(databaseSizeBeforeCreate + 1);
        Campanha testCampanha = campanhaList.get(campanhaList.size() - 1);
        assertThat(testCampanha.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testCampanha.getIdadeMinima()).isEqualTo(DEFAULT_IDADEMINIMA);
        assertThat(testCampanha.getIdadeMaxima()).isEqualTo(DEFAULT_IDADEMAXIMA);
        assertThat(testCampanha.getData_inicio()).isEqualTo(DEFAULT_DATA_INICIO);
        assertThat(testCampanha.getData_fim()).isEqualTo(DEFAULT_DATA_FIM);
        assertThat(testCampanha.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
    }

    @Test
    @Transactional
    public void createCampanhaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = campanhaRepository.findAll().size();

        // Create the Campanha with an existing ID
        Campanha existingCampanha = new Campanha();
        existingCampanha.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCampanhaMockMvc.perform(post("/api/campanhas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingCampanha)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Campanha> campanhaList = campanhaRepository.findAll();
        assertThat(campanhaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCampanhas() throws Exception {
        // Initialize the database
        campanhaRepository.saveAndFlush(campanha);

        // Get all the campanhaList
        restCampanhaMockMvc.perform(get("/api/campanhas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campanha.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].idadeMinima").value(hasItem(Integer.toString(DEFAULT_IDADEMINIMA))))
            .andExpect(jsonPath("$.[*].idadeMaxima").value(hasItem(Integer.toString(DEFAULT_IDADEMAXIMA))))
            .andExpect(jsonPath("$.[*].dataInicio").value(hasItem(DEFAULT_DATA_INICIO.toString())))
            .andExpect(jsonPath("$.[*].dataFim").value(hasItem(DEFAULT_DATA_FIM.toString())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO.toString())));
    }

    @Test
    @Transactional
    public void getCampanha() throws Exception {
        // Initialize the database
        campanhaRepository.saveAndFlush(campanha);

        // Get the campanha
        restCampanhaMockMvc.perform(get("/api/campanhas/{id}", campanha.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(campanha.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.idadeMinima").value(Integer.toString(DEFAULT_IDADEMINIMA)))
            .andExpect(jsonPath("$.idadeMaxima").value(Integer.toString(DEFAULT_IDADEMAXIMA)))
            .andExpect(jsonPath("$.dataInicio").value(DEFAULT_DATA_INICIO.toString()))
            .andExpect(jsonPath("$.dataFim").value(DEFAULT_DATA_FIM.toString()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCampanha() throws Exception {
        // Get the campanha
        restCampanhaMockMvc.perform(get("/api/campanhas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCampanha() throws Exception {
        // Initialize the database
        campanhaService.save(campanha);

        int databaseSizeBeforeUpdate = campanhaRepository.findAll().size();

        // Update the campanha
        Campanha updatedCampanha = campanhaRepository.findOne(campanha.getId());
        updatedCampanha
                .nome(UPDATED_NOME)
                .idadeMinima(UPDATED_IDADEMINIMA)
                .idadeMaxima(UPDATED_IDADEMAXIMA)
                .data_inicio(UPDATED_DATA_INICIO)
                .data_fim(UPDATED_DATA_FIM)
                .descricao(UPDATED_DESCRICAO);

        restCampanhaMockMvc.perform(put("/api/campanhas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCampanha)))
            .andExpect(status().isOk());

        // Validate the Campanha in the database
        List<Campanha> campanhaList = campanhaRepository.findAll();
        assertThat(campanhaList).hasSize(databaseSizeBeforeUpdate);
        Campanha testCampanha = campanhaList.get(campanhaList.size() - 1);
        assertThat(testCampanha.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testCampanha.getIdadeMinima()).isEqualTo(UPDATED_IDADEMINIMA);
        assertThat(testCampanha.getIdadeMaxima()).isEqualTo(UPDATED_IDADEMAXIMA);
        assertThat(testCampanha.getData_inicio()).isEqualTo(UPDATED_DATA_INICIO);
        assertThat(testCampanha.getData_fim()).isEqualTo(UPDATED_DATA_FIM);
        assertThat(testCampanha.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    public void updateNonExistingCampanha() throws Exception {
        int databaseSizeBeforeUpdate = campanhaRepository.findAll().size();

        // Create the Campanha

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCampanhaMockMvc.perform(put("/api/campanhas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campanha)))
            .andExpect(status().isCreated());

        // Validate the Campanha in the database
        List<Campanha> campanhaList = campanhaRepository.findAll();
        assertThat(campanhaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCampanha() throws Exception {
        // Initialize the database
        campanhaService.save(campanha);

        int databaseSizeBeforeDelete = campanhaRepository.findAll().size();

        // Get the campanha
        restCampanhaMockMvc.perform(delete("/api/campanhas/{id}", campanha.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Campanha> campanhaList = campanhaRepository.findAll();
        assertThat(campanhaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Campanha.class);
    }
}
