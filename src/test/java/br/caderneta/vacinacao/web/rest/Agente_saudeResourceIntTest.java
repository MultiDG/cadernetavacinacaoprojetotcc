package br.caderneta.vacinacao.web.rest;

import br.caderneta.vacinacao.CadernetavacinacaoApp;

import br.caderneta.vacinacao.domain.Agente_saude;
import br.caderneta.vacinacao.repository.Agente_saudeRepository;
import br.caderneta.vacinacao.service.Agente_saudeService;
import br.caderneta.vacinacao.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the Agente_saudeResource REST controller.
 *
 * @see Agente_saudeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CadernetavacinacaoApp.class)
public class Agente_saudeResourceIntTest {

    private static final String DEFAULT_NOME = "AAAAAAAAAA";
    private static final String UPDATED_NOME = "BBBBBBBBBB";

    private static final String DEFAULT_CPF = "AAAAAAAAAA";
    private static final String UPDATED_CPF = "BBBBBBBBBB";

    private static final String DEFAULT_CRM = "AAAAAAAAAA";
    private static final String UPDATED_CRM = "BBBBBBBBBB";

    private static final String DEFAULT_COREN = "AAAAAAAAAA";
    private static final String UPDATED_COREN = "BBBBBBBBBB";

    private static final String DEFAULT_INSCRICAO_MUNICIPAL = "AAAAAAAAAA";
    private static final String UPDATED_INSCRICAO_MUNICIPAL = "BBBBBBBBBB";

    @Autowired
    private Agente_saudeRepository agente_saudeRepository;

    @Autowired
    private Agente_saudeService agente_saudeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAgente_saudeMockMvc;

    private Agente_saude agente_saude;

//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        Agente_saudeResource agente_saudeResource = new Agente_saudeResource(agente_saudeService);
//        this.restAgente_saudeMockMvc = MockMvcBuilders.standaloneSetup(agente_saudeResource)
//            .setCustomArgumentResolvers(pageableArgumentResolver)
//            .setControllerAdvice(exceptionTranslator)
//            .setMessageConverters(jacksonMessageConverter).build();
//    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Agente_saude createEntity(EntityManager em) {
        Agente_saude agente_saude = new Agente_saude()
                .nome(DEFAULT_NOME)
                .cpf(DEFAULT_CPF)
                .crm(DEFAULT_CRM)
                .coren(DEFAULT_COREN)
                .inscricao_municipal(DEFAULT_INSCRICAO_MUNICIPAL);
        return agente_saude;
    }

    @Before
    public void initTest() {
        agente_saude = createEntity(em);
    }

    @Test
    @Transactional
    public void createAgente_saude() throws Exception {
        int databaseSizeBeforeCreate = agente_saudeRepository.findAll().size();

        // Create the Agente_saude

        restAgente_saudeMockMvc.perform(post("/api/agente-saudes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agente_saude)))
            .andExpect(status().isCreated());

        // Validate the Agente_saude in the database
        List<Agente_saude> agente_saudeList = agente_saudeRepository.findAll();
        assertThat(agente_saudeList).hasSize(databaseSizeBeforeCreate + 1);
        Agente_saude testAgente_saude = agente_saudeList.get(agente_saudeList.size() - 1);
        assertThat(testAgente_saude.getNome()).isEqualTo(DEFAULT_NOME);
        assertThat(testAgente_saude.getCpf()).isEqualTo(DEFAULT_CPF);
        assertThat(testAgente_saude.getCrm()).isEqualTo(DEFAULT_CRM);
        assertThat(testAgente_saude.getCoren()).isEqualTo(DEFAULT_COREN);
        assertThat(testAgente_saude.getInscricao_municipal()).isEqualTo(DEFAULT_INSCRICAO_MUNICIPAL);
    }

    @Test
    @Transactional
    public void createAgente_saudeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = agente_saudeRepository.findAll().size();

        // Create the Agente_saude with an existing ID
        Agente_saude existingAgente_saude = new Agente_saude();
        existingAgente_saude.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAgente_saudeMockMvc.perform(post("/api/agente-saudes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingAgente_saude)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Agente_saude> agente_saudeList = agente_saudeRepository.findAll();
        assertThat(agente_saudeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAgente_saudes() throws Exception {
        // Initialize the database
        agente_saudeRepository.saveAndFlush(agente_saude);

        // Get all the agente_saudeList
        restAgente_saudeMockMvc.perform(get("/api/agente-saudes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(agente_saude.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
            .andExpect(jsonPath("$.[*].cpf").value(hasItem(DEFAULT_CPF.toString())))
            .andExpect(jsonPath("$.[*].crm").value(hasItem(DEFAULT_CRM.toString())))
            .andExpect(jsonPath("$.[*].coren").value(hasItem(DEFAULT_COREN.toString())))
            .andExpect(jsonPath("$.[*].inscricao_municipal").value(hasItem(DEFAULT_INSCRICAO_MUNICIPAL.toString())));
    }

    @Test
    @Transactional
    public void getAgente_saude() throws Exception {
        // Initialize the database
        agente_saudeRepository.saveAndFlush(agente_saude);

        // Get the agente_saude
        restAgente_saudeMockMvc.perform(get("/api/agente-saudes/{id}", agente_saude.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(agente_saude.getId().intValue()))
            .andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
            .andExpect(jsonPath("$.cpf").value(DEFAULT_CPF.toString()))
            .andExpect(jsonPath("$.crm").value(DEFAULT_CRM.toString()))
            .andExpect(jsonPath("$.coren").value(DEFAULT_COREN.toString()))
            .andExpect(jsonPath("$.inscricao_municipal").value(DEFAULT_INSCRICAO_MUNICIPAL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAgente_saude() throws Exception {
        // Get the agente_saude
        restAgente_saudeMockMvc.perform(get("/api/agente-saudes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAgente_saude() throws Exception {
        // Initialize the database
        agente_saudeService.save(agente_saude);

        int databaseSizeBeforeUpdate = agente_saudeRepository.findAll().size();

        // Update the agente_saude
        Agente_saude updatedAgente_saude = agente_saudeRepository.findOne(agente_saude.getId());
        updatedAgente_saude
                .nome(UPDATED_NOME)
                .cpf(UPDATED_CPF)
                .crm(UPDATED_CRM)
                .coren(UPDATED_COREN)
                .inscricao_municipal(UPDATED_INSCRICAO_MUNICIPAL);

        restAgente_saudeMockMvc.perform(put("/api/agente-saudes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAgente_saude)))
            .andExpect(status().isOk());

        // Validate the Agente_saude in the database
        List<Agente_saude> agente_saudeList = agente_saudeRepository.findAll();
        assertThat(agente_saudeList).hasSize(databaseSizeBeforeUpdate);
        Agente_saude testAgente_saude = agente_saudeList.get(agente_saudeList.size() - 1);
        assertThat(testAgente_saude.getNome()).isEqualTo(UPDATED_NOME);
        assertThat(testAgente_saude.getCpf()).isEqualTo(UPDATED_CPF);
        assertThat(testAgente_saude.getCrm()).isEqualTo(UPDATED_CRM);
        assertThat(testAgente_saude.getCoren()).isEqualTo(UPDATED_COREN);
        assertThat(testAgente_saude.getInscricao_municipal()).isEqualTo(UPDATED_INSCRICAO_MUNICIPAL);
    }

    @Test
    @Transactional
    public void updateNonExistingAgente_saude() throws Exception {
        int databaseSizeBeforeUpdate = agente_saudeRepository.findAll().size();

        // Create the Agente_saude

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAgente_saudeMockMvc.perform(put("/api/agente-saudes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(agente_saude)))
            .andExpect(status().isCreated());

        // Validate the Agente_saude in the database
        List<Agente_saude> agente_saudeList = agente_saudeRepository.findAll();
        assertThat(agente_saudeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAgente_saude() throws Exception {
        // Initialize the database
        agente_saudeService.save(agente_saude);

        int databaseSizeBeforeDelete = agente_saudeRepository.findAll().size();

        // Get the agente_saude
        restAgente_saudeMockMvc.perform(delete("/api/agente-saudes/{id}", agente_saude.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Agente_saude> agente_saudeList = agente_saudeRepository.findAll();
        assertThat(agente_saudeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Agente_saude.class);
    }
}
