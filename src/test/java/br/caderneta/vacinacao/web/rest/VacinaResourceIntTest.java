package br.caderneta.vacinacao.web.rest;

import br.caderneta.vacinacao.CadernetavacinacaoApp;

import br.caderneta.vacinacao.domain.Vacina;
import br.caderneta.vacinacao.repository.PacienteRepository;
import br.caderneta.vacinacao.repository.VacinaRepository;
import br.caderneta.vacinacao.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VacinaResource REST controller.
 *
 * @see VacinaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CadernetavacinacaoApp.class)
public class VacinaResourceIntTest {

    private static final String DEFAULT_NOME_VACINA = "AAAAAAAAAA";
    private static final String UPDATED_NOME_VACINA = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATA_VACINA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATA_VACINA = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_PROXIMA_VACINA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PROXIMA_VACINA = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_LOTE = "AAAAAAAAAA";
    private static final String UPDATED_LOTE = "BBBBBBBBBB";

    private static final String DEFAULT_LOCAL_VACINA = "AAAAAAAAAA";
    private static final String UPDATED_LOCAL_VACINA = "BBBBBBBBBB";

    @Autowired
    private VacinaRepository vacinaRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restVacinaMockMvc;

    private Vacina vacina;
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        VacinaResource vacinaResource = new VacinaResource(vacinaRepository);
        this.restVacinaMockMvc = MockMvcBuilders.standaloneSetup(vacinaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vacina createEntity(EntityManager em) {
        Vacina vacina = new Vacina()
                .nome_vacina(DEFAULT_NOME_VACINA)
                .data_vacina(DEFAULT_DATA_VACINA)
                .proxima_vacina(DEFAULT_PROXIMA_VACINA)
                .lote(DEFAULT_LOTE)
                .local_vacina(DEFAULT_LOCAL_VACINA);
        return vacina;
    }

    @Before
    public void initTest() {
        vacina = createEntity(em);
    }

    @Test
    @Transactional
    public void createVacina() throws Exception {
        int databaseSizeBeforeCreate = vacinaRepository.findAll().size();

        // Create the Vacina

        restVacinaMockMvc.perform(post("/api/vacinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacina)))
            .andExpect(status().isCreated());

        // Validate the Vacina in the database
        List<Vacina> vacinaList = vacinaRepository.findAll();
        assertThat(vacinaList).hasSize(databaseSizeBeforeCreate + 1);
        Vacina testVacina = vacinaList.get(vacinaList.size() - 1);
        assertThat(testVacina.getNome_vacina()).isEqualTo(DEFAULT_NOME_VACINA);
        assertThat(testVacina.getData_vacina()).isEqualTo(DEFAULT_DATA_VACINA);
        assertThat(testVacina.getProxima_vacina()).isEqualTo(DEFAULT_PROXIMA_VACINA);
        assertThat(testVacina.getLote()).isEqualTo(DEFAULT_LOTE);
        assertThat(testVacina.getLocal_vacina()).isEqualTo(DEFAULT_LOCAL_VACINA);
    }

    @Test
    @Transactional
    public void createVacinaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vacinaRepository.findAll().size();

        // Create the Vacina with an existing ID
        Vacina existingVacina = new Vacina();
        existingVacina.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVacinaMockMvc.perform(post("/api/vacinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingVacina)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Vacina> vacinaList = vacinaRepository.findAll();
        assertThat(vacinaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNome_vacinaIsRequired() throws Exception {
        int databaseSizeBeforeTest = vacinaRepository.findAll().size();
        // set the field null
        vacina.setNome_vacina(null);

        // Create the Vacina, which fails.

        restVacinaMockMvc.perform(post("/api/vacinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacina)))
            .andExpect(status().isBadRequest());

        List<Vacina> vacinaList = vacinaRepository.findAll();
        assertThat(vacinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkData_vacinaIsRequired() throws Exception {
        int databaseSizeBeforeTest = vacinaRepository.findAll().size();
        // set the field null
        vacina.setData_vacina(null);

        // Create the Vacina, which fails.

        restVacinaMockMvc.perform(post("/api/vacinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacina)))
            .andExpect(status().isBadRequest());

        List<Vacina> vacinaList = vacinaRepository.findAll();
        assertThat(vacinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLoteIsRequired() throws Exception {
        int databaseSizeBeforeTest = vacinaRepository.findAll().size();
        // set the field null
        vacina.setLote(null);

        // Create the Vacina, which fails.

        restVacinaMockMvc.perform(post("/api/vacinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacina)))
            .andExpect(status().isBadRequest());

        List<Vacina> vacinaList = vacinaRepository.findAll();
        assertThat(vacinaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVacinas() throws Exception {
        // Initialize the database
        vacinaRepository.saveAndFlush(vacina);

        // Get all the vacinaList
        restVacinaMockMvc.perform(get("/api/vacinas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vacina.getId().intValue())))
            .andExpect(jsonPath("$.[*].nome_vacina").value(hasItem(DEFAULT_NOME_VACINA.toString())))
            .andExpect(jsonPath("$.[*].data_vacina").value(hasItem(DEFAULT_DATA_VACINA.toString())))
            .andExpect(jsonPath("$.[*].proxima_vacina").value(hasItem(DEFAULT_PROXIMA_VACINA.toString())))
            .andExpect(jsonPath("$.[*].lote").value(hasItem(DEFAULT_LOTE.toString())))
            .andExpect(jsonPath("$.[*].local_vacina").value(hasItem(DEFAULT_LOCAL_VACINA.toString())));
    }

    @Test
    @Transactional
    public void getVacina() throws Exception {
        // Initialize the database
        vacinaRepository.saveAndFlush(vacina);

        // Get the vacina
        restVacinaMockMvc.perform(get("/api/vacinas/{id}", vacina.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vacina.getId().intValue()))
            .andExpect(jsonPath("$.nome_vacina").value(DEFAULT_NOME_VACINA.toString()))
            .andExpect(jsonPath("$.data_vacina").value(DEFAULT_DATA_VACINA.toString()))
            .andExpect(jsonPath("$.proxima_vacina").value(DEFAULT_PROXIMA_VACINA.toString()))
            .andExpect(jsonPath("$.lote").value(DEFAULT_LOTE.toString()))
            .andExpect(jsonPath("$.local_vacina").value(DEFAULT_LOCAL_VACINA.toString()));
    }


    @Test
    @Transactional
    public void getNonExistingVacina() throws Exception {
        // Get the vacina
        restVacinaMockMvc.perform(get("/api/vacinas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVacina() throws Exception {
        // Initialize the database
        vacinaRepository.saveAndFlush(vacina);
        int databaseSizeBeforeUpdate = vacinaRepository.findAll().size();

        // Update the vacina
        Vacina updatedVacina = vacinaRepository.findOne(vacina.getId());
        updatedVacina
                .nome_vacina(UPDATED_NOME_VACINA)
                .data_vacina(UPDATED_DATA_VACINA)
                .proxima_vacina(UPDATED_PROXIMA_VACINA)
                .lote(UPDATED_LOTE)
                .local_vacina(UPDATED_LOCAL_VACINA);

        restVacinaMockMvc.perform(put("/api/vacinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedVacina)))
            .andExpect(status().isOk());

        // Validate the Vacina in the database
        List<Vacina> vacinaList = vacinaRepository.findAll();
        assertThat(vacinaList).hasSize(databaseSizeBeforeUpdate);
        Vacina testVacina = vacinaList.get(vacinaList.size() - 1);
        assertThat(testVacina.getNome_vacina()).isEqualTo(UPDATED_NOME_VACINA);
        assertThat(testVacina.getData_vacina()).isEqualTo(UPDATED_DATA_VACINA);
        assertThat(testVacina.getProxima_vacina()).isEqualTo(UPDATED_PROXIMA_VACINA);
        assertThat(testVacina.getLote()).isEqualTo(UPDATED_LOTE);
        assertThat(testVacina.getLocal_vacina()).isEqualTo(UPDATED_LOCAL_VACINA);
    }

    @Test
    @Transactional
    public void updateNonExistingVacina() throws Exception {
        int databaseSizeBeforeUpdate = vacinaRepository.findAll().size();

        // Create the Vacina

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVacinaMockMvc.perform(put("/api/vacinas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacina)))
            .andExpect(status().isCreated());

        // Validate the Vacina in the database
        List<Vacina> vacinaList = vacinaRepository.findAll();
        assertThat(vacinaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteVacina() throws Exception {
        // Initialize the database
        vacinaRepository.saveAndFlush(vacina);
        int databaseSizeBeforeDelete = vacinaRepository.findAll().size();

        // Get the vacina
        restVacinaMockMvc.perform(delete("/api/vacinas/{id}", vacina.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Vacina> vacinaList = vacinaRepository.findAll();
        assertThat(vacinaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Vacina.class);
    }
}
